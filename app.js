
import { randomUserMock } from "./FE4U-Lab2-mock.js";
import { additionalUsers } from "./FE4U-Lab2-mock.js";

// task 1
function formatRandomUserMock() {
    let newRandomUserMock = [];
    randomUserMock.forEach(element => {
        newRandomUserMock.push(
            {
                "gender": element['gender'],
                "title": element['name']['title'],
                "full_name": element['name']['first'] + " " + element['name']['last'],
                "city": element['location']['city'],
                "state": element['location']['state'],
                "country": element['location']['country'],
                "postcode": element['location']['postcode'],
                "coordinates": { "latitude": element['location']['coordinates']['latitude'], "longitude": element['location']['coordinates']['longitude'] },
                "timezone": { "offset": element['location']['timezone']['offset'], "description": element['location']['timezone']['description'] },
                "email": element['email'],
                "b_date": element['dob']['date'],
                "age": element['dob']['age'],
                "phone": element['phone'],
                "picture_large": element['picture']['large'],
                "picture_thumbnail": element['picture']['thumbnail'],
                id: generateRandomString(17),
                favorite: generateRandomBoolean(0.2),
                course: getRandomItemFromArray(),
                bg_color: generateRandomColorCode(),
                note: "random text",
            }
        );
    });

    additionalUsers.forEach(element => {
        if (newRandomUserMock.some(function (element2) {
            return element2.full_name === element.full_name;
        })) {
            // no push
        }
        else {
            newRandomUserMock.push(element);
        }

    });

    // console.log(newRandomUserMock);
    return newRandomUserMock;
}

function capitalizeFirstLetter(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function generateRandomString(length) {
    const characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let randomString = '';

    for (let i = 0; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * characters.length);
        randomString += characters.charAt(randomIndex);
    }

    return randomString;
}

function generateRandomBoolean(chanceOfTrue) {
    const randomValue = Math.random();
    return randomValue < chanceOfTrue;
}

function getRandomItemFromArray() {
    const topics = [
        'Mathematics', 'Physics', 'English', 'Computer Science',
        'Dancing', 'Chess', 'Biology', 'Chemistry',
        'Law', 'Art', 'Medicine', 'Statistics'
    ];
    const randomIndex = Math.floor(Math.random() * topics.length);
    return topics[randomIndex];
}



function generateRandomColorCode() {
    const characters = '0123456789ABCDEF';
    let colorCode = '#';

    for (let i = 0; i < 6; i++) {
        const randomIndex = Math.floor(Math.random() * characters.length);
        colorCode += characters.charAt(randomIndex);
    }

    return colorCode;
}

// task 2

function validate_data(data) {
    let k = 0;
    let validatedUserMock = [];
    data.forEach(element => {
        let flag = true;
        if (!(isUpperCase(element.full_name) && isUpperCase(element.state) && isUpperCase(element.city) && isUpperCase(element.country))) {
            flag = false;
            console.log("full_name, note, state, city, country must be strings, and start with a capital letter")
        }
        if(!isLowerCase(element.gender)){
            flag = false;
            console.log("gender must start with a lower case letter")
        }
        if (!(typeof element.age === 'number' || !isNaN(element.age))) {
            console.log("The age field must be numeric.");
            flag = false;
        }
        if (!(isValidPhoneNumber(element.phone))) {
            console.log("invalid phone format");
            flag = false;
        }
        if (!(typeof element.email === 'string' && element.email.includes('@'))) {
            console.log("The email field must have @");
            flag = false;
        }
        if (element.favorite === null || element.note === null || element.course === null || element.bg_color === null) {
            console.log("the element has null attribute");
            flag = false;
        }
        else if (isUpperCase(element.note)) {
            flag = false;
            console.log("note should be string, and starts with a capital letter")
        }
        if (!flag) {
            console.log("the element is invalid")
            console.log(element);
        }
        else {
            k++;
            validatedUserMock.push(element);
        }
    });
    console.log(k + " elements is valid");
    return validatedUserMock;
}

function isUpperCase(str) {
    if (typeof(str) !== 'string') return false;
    return str.charAt(0) === str.charAt(0).toUpperCase();
}
function isLowerCase(str) {
    if (typeof(str) !== 'string') return false;
    return str.charAt(0) === str.charAt(0).toLowerCase();
}
function isValidPhoneNumber(phoneNumber) {
    // const regex = /^(?!-)[\d\s()-]*\d[\d\s()-]*(?<!-)$/;
    const regex = /^[\d\s\(\)]*[\d\s\-\(\)]+[\d\s\(\)]*$/;
    if (regex.test(phoneNumber)) {
        let flag = true;
        let k = 0;
        for (let i = 0; i < phoneNumber.length; i++) {
            const ch = phoneNumber[i];
            if (ch === '(') k++;
            else if (ch === ')') {
                if (k == 0) {
                    flag = false;
                    break;
                }
                else k--;
            }
        }
        if (flag && k == 0) return true;
    }
    return false;
}

// task 3

function filterArray(array, filter) {
    return array.filter(item => {
        for (let key in filter) {
            if (filter.hasOwnProperty(key)) {
                if (Array.isArray(filter[key])) {
                    // Фільтрація за масивом
                    if (!filter[key].includes(item[key])) {
                        return false;
                    }
                } else if (typeof filter[key] === 'object' && filter[key].hasOwnProperty('min') && filter[key].hasOwnProperty('max')) {
                    // Фільтрація за проміжком
                    if (item[key] < filter[key].min || item[key] > filter[key].max) {
                        return false;
                    }
                } else {
                    // Фільтрація за рівністю
                    if (item[key] !== filter[key]) {
                        return false;
                    }
                }
            }
        }
        return true;
    });
}

// task 4

function sortObjects(array, sortBy, sortOrder) {
    let newArray = array.slice();
    newArray.sort((a, b) => {
        if (typeof a[sortBy] === 'number' && typeof b[sortBy] === 'number') {
            // Якщо поле є числовим
            return sortOrder === 'asc' ? a[sortBy] - b[sortBy] : b[sortBy] - a[sortBy];
        } else {
            // Якщо поле є строковим
            return sortOrder === 'asc' ? a[sortBy].localeCompare(b[sortBy]) : b[sortBy].localeCompare(a[sortBy]);
        }
    });
    return newArray;
}

// task 5

function findObjectsByParam(array, param, value) {
    return array.filter(obj => {
      if (param !== 'age') {
        return obj[param].toLowerCase().includes(value.toLowerCase());
      } else {
        return obj[param] === value;
      }
    });
  }

// task 6

function calculatePercentageMatching(array, filter) {
    const filteredArray = filterArray(array, filter);
    // console.log(filteredArray);

    var percentage = (filteredArray.length / array.length);

    return percentage;
}

// tester

// task1
const newRandomUserMock = formatRandomUserMock();
console.log('task1: format randomUserMock');
console.log(newRandomUserMock);

// task2
console.log('task2: validate data')
const validatedNewUserMock = validate_data(newRandomUserMock);

// task3
let filterCriteria = {
    country: ["Switzerland", 'Germany', "Denmark"],
    age: { min: 25, max: 66 },
    gender: 'male',
    favorite: false,
};

var filteredData = filterArray(validatedNewUserMock, filterCriteria);

console.log('task3')
console.log("filtered array: ")
console.log(filteredData);
filterCriteria = {
    country: "Norway",
    age: { min: 25, max: 66 },
    gender: 'male',
    favorite: [false, true],
};

filteredData = filterArray(validatedNewUserMock, filterCriteria);
console.log("filtered array: ")
console.log(filteredData);

// task 4
console.log('task4')
console.log('Сортування за зростанням віку');
var sortedData = sortObjects(validatedNewUserMock, 'age', 'asc');
console.log(sortedData);

console.log('Сортування за зростанням віку');
var sortedDataDesc = sortObjects(validatedNewUserMock, 'age', 'desc');
console.log(sortedDataDesc);

console.log('Сортування за зростанням країни');
var sortedData = sortObjects(validatedNewUserMock, 'country', 'asc');
console.log(sortedData);

// task 5
console.log('task5')
console.log(findObjectsByParam(validatedNewUserMock, 'age', 35));

console.log(findObjectsByParam(validatedNewUserMock, 'full_name', 'Ma'));

// task 6
console.log('task6')
console.log(calculatePercentageMatching(validatedNewUserMock, {age: {min: 35, max:1000}})*100+"%");